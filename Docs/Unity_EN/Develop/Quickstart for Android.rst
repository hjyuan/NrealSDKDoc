.. role:: raw-html-m2r(raw)
   :format: html

.. _quickstart_android:

Quickstart for Android
----------------------

:raw-html-m2r:`<font color=#636363>Start developing your NRSDK Unity apps on Android.</font>`

:raw-html-m2r:`<font color=#636363>This quickstart guide will help you set up your development environment and test out the sample app "Hello MR" on NRSDK.</font>`

:raw-html-m2r:`<br />`


.. image:: ../../../images/unity01.jpg


Getting Started
^^^^^^^^^^^^^^^

**Hardware Checklist**


* 
  :raw-html-m2r:`<font color=#636363>A Nreal Computing Unit (Think of it as an Android phone with no screen, so all development processes will be very similar to mobile app development.)</font>`

* 
  :raw-html-m2r:`<font color=#636363>A pair of Nreal Light glasses</font>`

..

   Don't have an Nreal device? Sign up for the `Nreal Developer Kit </app/apply>`_\ ! Or try **Emulator**  to pilot Nreal app functions without Nreal Light glasses and computing unit.



* 
  :raw-html-m2r:`<font color=#636363>A USB-C cable to connect the Nreal computing unit to your PC.</font>`

* Wi-Fi connection is not necessary. However, a Wi-Fi **Android Debug Bridge** `(adb) <https://developer.android.com/studio/command-line/adb>`_ connection can be used to debug and test.

**Software Checklist**

* 
  `Unity 2018.2.X or later <https://unity3d.com/get-unity/download>`_ with Android Build Support 
    
  (It is recommended to use version 2018.4.11f1, 2019.4.26f1, 2020.3.8f1.)

* 
  :raw-html-m2r:`<font color=#636363>Download</font>` `NRSDKForUnity_1.6.0 <https://nreal-public.nreal.ai/download/NRSDKForUnityAndroid_1.6.0.unitypackage>`_

  :raw-html-m2r:`<font color=#636363>The SDK is downloaded as</font>` ``NRSDKForUnity_1.6.0.unitypackage``

* Android SDK 8.0 (API Level 26) or later, installed using the SDK Manager in `Android Studio <https://developer.android.com/studio>`_.

:raw-html-m2r:`<br />`
  

Creating a Unity Project
^^^^^^^^^^^^^^^^^^^^^^^^


* 
  :raw-html-m2r:`<font color=#636363>Open Unity and create a new 3D project.</font>`

* 
  Set ``Player Settings>Other Settings>Scritping Runtime Version``  to  ``.net 4.x`` equivalent

* 
  :raw-html-m2r:`<font color=#636363>Import NRSDK for Unity</font>`


  * 
    Select ``Assets>Import Package>Custom`` Package.

  * 
    Select the ``NRSDKForUnity_1.4.8.unitypackage`` that you downloaded.

  * :raw-html-m2r:`<font color=#636363>In the</font>` **Importing** :raw-html-m2r:`<font color=#636363>Package dialog, make sure that all package options are selected and click</font>` **Import**.

:raw-html-m2r:`<br />`

Hello MR - Your First Sample App
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* 
  :raw-html-m2r:`<font color=#636363>Find the</font>` **HelloMR** :raw-html-m2r:`<font color=#636363>sample app in the Unity Project window by selecting `Assets > NRSDK > Demos > HelloMR.`</font>`

    
  .. image:: ../../../images/unity02.jpg


:raw-html-m2r:`<br />`

Configure Build Settings
^^^^^^^^^^^^^^^^^^^^^^^^


* :raw-html-m2r:`<font color=#636363>Go to</font>` **File > Build Settings**.
* :raw-html-m2r:`<font color=#636363>Select</font>` **Android** :raw-html-m2r:`<font color=#636363>and click</font>` **Switch Platform**.
* :raw-html-m2r:`<font color=#636363>In the</font>` **Build Settings** :raw-html-m2r:`<font color=#636363>window, click</font>` **Player Settings**.
* :raw-html-m2r:`<font color=#636363>In the</font>` **Inspector** :raw-html-m2r:`<font color=#636363>window, configure player settings as follows:</font>`

.. list-table::
   :header-rows: 1

   * - Setting
     - Value
   * - ``Player Settings > Resolution and Presentation > Default Orientation``
     - **Portrait**
   * - ``Player Settings > Other Settings > Auto Graphics API``
     - :raw-html-m2r:`<font color=#636363>false</font>`
   * - ``Player Settings > Other Settings > Graphics APIs``
     - :raw-html-m2r:`<font color=#636363>OpenGLES3</font>`
   * - ``Player Settings > Other Settings > Package Name``
     - :raw-html-m2r:`<font color=#636363>Create a unique app ID using a Java package name format. For example, use</font>` **com.nreal.helloMR**
   * - ``Player Settings > Other Settings > Minimum API Level``
     - :raw-html-m2r:`<font color=#636363>Android 8.0 or higher</font>`
   * - ``Player Settings > Other Settings > Target API Level``
     - :raw-html-m2r:`<font color=#636363>Android 8.0 or higher</font>`
   * - ``Player Settings > Other Settings > Write Permission``
     - :raw-html-m2r:`<font color=#636363>External(SDCard)</font>`
   * - ``Project Settings > Quality > V Sync Count``
     - :raw-html-m2r:`<font color=#636363>Don't Sync</font>`


:raw-html-m2r:`<br />`

Connect to Nreal Device
^^^^^^^^^^^^^^^^^^^^^^^


* Enable developer options and USB debugging on your computing unit. **Android Debug Bridge** `(adb) <https://developer.android.com/studio/command-line/adb>`_  is enabled as default and does not require manual setting).

* 
  :raw-html-m2r:`<font color=#636363>Connect your computing unit to your Windows PC.</font>`

:raw-html-m2r:`<br />`

Build and Run
^^^^^^^^^^^^^


* 
  :raw-html-m2r:`<font color=#636363>In the Unity</font>` **Build Settings** :raw-html-m2r:`<font color=#636363>window, click</font>` **Build**\ . Install your app through WiFi **Android Debug Bridge** `(adb) <https://developer.android.com/studio/command-line/adb>`_ after the build is successful.

* 
  :raw-html-m2r:`<font color=#636363>Disconnect the computing unit with your PC, and then connect it to the glasses.</font>`

* If it is the first time you run this app, you need to authrize the app by some tools like `scrcpy <https://github.com/Genymobile/scrcpy>`_.

* Launch you app along with the Nreal Light controller. For instructions on how to use the Nreal Light controller, please refer to :ref:`Controller<controller_guide>`.

* 
  :raw-html-m2r:`<font color=#636363>Move around until NRSDK finds a horizontal plane and the detected plane will be covered with green grid.</font>`

* 
  :raw-html-m2r:`<font color=#636363>Click the Trigger button to put an Nreal logo object on it.</font>`

* (Optional) Use **Android Logcat** to view logged messages. We recommend using WiFi **Android Debug Bridge** `(adb) <https://developer.android.com/studio/command-line/adb>`_ to connect to your PC so that you do not have to be connected through the data cable most of the time.

:raw-html-m2r:`<br />`


NRSDK Coordinate System for Plugins
===================

This document describes the definition of coordinate systems of the Nreal glass components. Please note that this document is not for application developers (upward rendering purposes), but for third-party software solution providers (downward plugins).


Glass Components and Coordinate Systems
-------------

- The Nreal glass consists of the following key components

    - 2 x Grayscale Cameras
    - 2 x Display Cameras
    - Head / IMU
    - RGB Camera


- The placement of the above components and their corresponding coordinate systems, as defined in NRSDK, are as follows |image0|



Interface for Head(IMU) Pose
^^^^^^^^^^^^^^^^^^^^^
- Interface function: This function returns the 6dof pose, of the target_device in the source_device. 

.. code-block:: c#
     :linenos:

        // head pose(IMU,"NRCameraRig" transform in Unity)
        Pose headpose = NRFrame.HeadPose;


- Enum for component (device) names

.. code-block:: C#
     :linenos:

        public enum NativeDevice
        {
            /// <summary> 
            /// Left display. 
            /// </summary>
            LEFT_DISPLAY = 0,

            /// <summary> 
            /// Right display. 
            /// </summary>
            RIGHT_DISPLAY = 1,

            /// <summary> 
            /// RGB camera. 
            /// </summary>
            RGB_CAMERA = 2,

            /// <summary> 
            ///  The left grayscale camera.
            ///  Only supported in the version of preview.
            /// </summary>
            LEFT_GRAYSCALE_CAMERA = 3,

            /// <summary> 
            ///  The right grayscale camera.
            ///  Only supported in the version of preview.
            /// </summary>
            RIGHT_GRAYSCALE_CAMERA = 4,
        }


**Example 1: Getting the Pose Between the Head/IMU and the Left Display, Right Display, RGB Camera**

.. code-block:: C#
     :linenos:

        // camera external parameters
        var eyePoseFromHead = NRFrame.EyePoseFromHead;
        
        // left display local pose from "NRCameraRig"
        Pose left_display = eyePoseFromHead.LEyePose;

        // right display local pose from "NRCameraRig"
        Pose right_display = eyePoseFromHead.REyePose;

        // rgb camera local pose from "NRCameraRig"
        Pose rgb_camera = eyePoseFromHead.RGBEyePose;


**Example 2: Getting the Extrinsics Between the Head/IMU and the Left Grayscale Camera**

.. code-block:: C#
     :linenos:

        // This interface only supported in the version of Preview, not release version
        Pose leyeGrayEyePose = NRFrameExtension.GrayEyePoseFromHead.LEyePose;

|image2|


Head/IMU Pose in Global Coordinate System
----------------------

- The global coordinate frame of the tracking system is as follows
|image3|

Camera Coordinate System (OpenGL)
----------------------
- The definition of the camera's coordinate frame in the NRSDK follows the OpenGL convention, i.e. X-right, Y-upward, Z-backward. For developing and using computer vision algorithms, you may need to convert the camera coordinates to the OpenCV convention, which simply requires negating the Y and Z coordinates.
|image4|


Image Pixel Coordinate System and Camera Intrinsics (OpenCV)
----------------------
- The definition of the image pixel coordinates and the camera intrinsics in the NRSDK follows the OpenCV convention. The image data is stored row wise in memory.
|image5|



- Interface for getting the camera's intrinsic parameters

.. code-block:: C#
     :linenos:

        NRFrame.GetDeviceIntrinsicMatrix(NativeDevice device);


Wireframes
---------
|image6|
|image7|
|image8|



.. |image0| image:: ../../../images/CoordinateSystem/img01.png
.. |image1| image:: ../../../images/CoordinateSystem/img02.png
.. |image2| image:: ../../../images/CoordinateSystem/img03.png
.. |image3| image:: ../../../images/CoordinateSystem/img04.png
.. |image4| image:: ../../../images/CoordinateSystem/img05.png
.. |image5| image:: ../../../images/CoordinateSystem/img06.png
.. |image6| image:: ../../../images/CoordinateSystem/img07.png
.. |image7| image:: ../../../images/CoordinateSystem/img08.png
.. |image8| image:: ../../../images/CoordinateSystem/img09.png
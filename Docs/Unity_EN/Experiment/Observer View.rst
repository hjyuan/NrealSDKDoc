.. role:: raw-html-m2r(raw)
   :format: html


Observer View
-------------


Overview
^^^^^^^^

:raw-html-m2r:`<font color=#636363>NRSDK's observer view allows users to share their mixed reality experience with others who are holding a phone or tablet running ARCore. These shared experiences can be streamed or recorded.</font>`


.. image:: https://codimd.s3.shivering-isles.com/demo/uploads/upload_1781b8b5624ba6a627c6fbb5cca08a93.jpg
   :target: https://codimd.s3.shivering-isles.com/demo/uploads/upload_1781b8b5624ba6a627c6fbb5cca08a93.jpg
   :alt: 


:raw-html-m2r:`<br />`

Capabilities
~~~~~~~~~~~~


* :raw-html-m2r:`<font color=#636363>Camera movement supported.</font>`   
* :raw-html-m2r:`<font color=#636363>Third-person view and multi-user experience supported; Viewers can interact.</font>`
* :raw-html-m2r:`<font color=#636363>Can be streamed to screens.</font>`   
* :raw-html-m2r:`<font color=#636363>Android phone (ARCore supported) required.</font>`  
* :raw-html-m2r:`<font color=#636363>Wireless supported / Networking required (UNET scripting); Network server auto-discovery for adding phones to sessions.</font>`
* :raw-html-m2r:`<font color=#636363>A set of synchronized network frames that supports value synchronization and RPC function.</font>`
* :raw-html-m2r:`<font color=#636363>Coordinate system alignment allows everyone to see Nreal applications in the exact same place.</font>`

Use Cases
~~~~~~~~~


* :raw-html-m2r:`<font color=#636363>Record mixed reality in HD: Using Observer View, you can record a mixed reality experience using an Android phone or a tablet.</font>`
* :raw-html-m2r:`<font color=#636363>Live demonstrations: Stream live mixed reality experiences to a TV directly from your phone or tablet.</font>`
* :raw-html-m2r:`<font color=#636363>Share your experience with others: Have others share your mixed reality experience on the spot from their phones or tablets.</font>`

Licenses
~~~~~~~~


* :raw-html-m2r:`<font color=#636363>Unity ARCore - (Apache 2.0 License)</font>`

:raw-html-m2r:`<br />`

Tutorial: How to Set Up Observer View
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Requirements
~~~~~~~~~~~~


* 
  :raw-html-m2r:`<font color=#636363>Observer View uses the built-in network for its network discovery and spatial syncing. This means all interactivity during the application needs to be synced between devices.</font>`

* 
  :raw-html-m2r:`<font color=#636363>Hardware</font>`


  * :raw-html-m2r:`<font color=#636363>Nreal Light glasses</font>`
  * :raw-html-m2r:`<font color=#636363>Windows PC running Windows 10</font>`
  * :raw-html-m2r:`<font color=#636363>[ARCore compatible device](https://developers.google.com/ar/discover/supported-devices) </font>`

Network Synchronization
~~~~~~~~~~~~~~~~~~~~~~~

 :raw-html-m2r:`<font color=#636363>All network synchronization is based on NetworkBehaviour.  See</font>` ``CameraCaptureController.cs`` :raw-html-m2r:`<font color=#636363>, located in</font>` ``Assets > NRSDK > Demos > Sharring > Scripts > TestNetBehaviour`` :raw-html-m2r:`<font color=#636363>for an example on how to realize your NetworkBehaviour. </font>`

:raw-html-m2r:`<font color=#636363>Your own NetworkBehaviour class must inherit the NetworkBehaviour base class.</font>`

**Data Synchronization** :raw-html-m2r:`<font color=#636363>The SDK comes with several data types that can be synchronized. They are the following:</font>`


* :raw-html-m2r:`<font color=#636363>SynTransform</font>`
* :raw-html-m2r:`<font color=#636363>SynInt</font>`
* :raw-html-m2r:`<font color=#636363>SynVector2</font>`
* :raw-html-m2r:`<font color=#636363>SynVector3</font>`
* :raw-html-m2r:`<font color=#636363>SynQuaternion</font>`
* 
  :raw-html-m2r:`<font color=#636363>*RPC Function* * You can use RPC functions as below:</font>`

  .. code-block:: c#

     this.RPC("Hello");

     public void Hello()
     {
         Debug.Log("Hello world!");
     }

Coordinate Alignment
~~~~~~~~~~~~~~~~~~~~


* 
  :raw-html-m2r:`<font color=#636363>Add a parent transform for "NRCameraRig" named "CameraRoot"</font>`

* 
  :raw-html-m2r:`<font color=#636363>See</font>` ``UpdateWorldOrigin.cs`` :raw-html-m2r:`<font color=#636363>, located in</font>` ``Assets > NRSDK > Scripts > Utility``  :raw-html-m2r:`<font color=#636363>You can use UpdateWorldOrigin to align the coordinate of your detected image as below:</font>`

  .. code-block:: c#

      UpdateWorldOrigin.ResetWorldOrigin(cameraRoot,position, rotation);

Setup Application
~~~~~~~~~~~~~~~~~


* 
  :raw-html-m2r:`<font color=#636363>Click</font>` ``NRWindows > Start Sharing Server`` :raw-html-m2r:`<font color=#636363>menu to initiate a local sharing server.</font>`

* 
  :raw-html-m2r:`<font color=#636363>Start your client applications to search for and connect to local servers.</font>`

* 
  :raw-html-m2r:`<font color=#636363>Scan your tracking image to align coordinates. All applications will share the same coordinates. The data will synchronize subsequently.</font>` 

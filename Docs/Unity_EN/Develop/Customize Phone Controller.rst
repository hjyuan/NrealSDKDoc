.. role:: raw-html-m2r(raw)
   :format: html


Customize Phone Controller
--------------------------

Intro
^^^^^

:raw-html-m2r:`<font color=#636363>When we release the Nreal Light glasses to consumers, this will only include the glasses and not the computing unit that was part of the dev kit. You will be able to plug Nreal Light into your own Android phone and use the phone as a 3DOF controller. </font>`

.. image:: ../../../images/cpc1.jpg

:raw-html-m2r:`<font color=#636363>So if you want to create unique experiences that stand out from the rest you can fully customize your MR app's phone controller interface.</font>`
:raw-html-m2r:`<font color=#636363>And if you are using the developer kit computing unit, you can use the Nreal Light controller.</font>`

Notice
^^^^^^


* **This feature is only available in NRSDK ver. 1.2.0 and above.**
* **This feature is for mobile phones, but you could still use the computing unit and preview your customised controller interface with scrcpy.**
* :raw-html-m2r:`<font color=#636363>If you have built somethings before NRSDK ver. 1.2.0, please be aware that the Home Key and App Key have been interchanged. </font>`

How to use a phone as controller
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controller Features Description
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* :raw-html-m2r:`<font color=#636363>3DoF Tracking</font>`


* :raw-html-m2r:`<font color=#636363>Touchpad</font>`
    :raw-html-m2r:`<font color=#636363>Represents "Trigger" button，and is also used as a touchpad. It can detect touching and swipping.</font>`

* :raw-html-m2r:`<font color=#636363>Home Button</font>`
    :raw-html-m2r:`<font color=#636363>Click to call "Exit App" menu. Press and hold to recenter your controller. We would advise developers to not use this button when developing apps.</font>`

* :raw-html-m2r:`<font color=#636363>App Button</font>`
    :raw-html-m2r:`<font color=#636363>Can be freely customized for your app, e.g. click to open an in-app menu or perform a special action.</font>`

Two ways to use
~~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>One of the great things about the Nreal phone controller is that you could easily customise it's UI interrace. Here are two ways to use Nreal phone controller:</font>`


* :raw-html-m2r:`<font color=#636363>The default way</font>`
  :raw-html-m2r:`<font color=#636363>Simply drag the "NRInput" prefab into your scene. Then when you run the app on your device, a default virtual controller("NRVirtualDisplayer" prefab) will be automatically loaded. It has 3 common controller buttons(Trigger, Home, App) on it. This virutal controller is very similar to Nreal Light controller.</font>`

* :raw-html-m2r:`<font color=#636363>The custom way</font>`
  :raw-html-m2r:`<font color=#636363>Drag the "NRInput" prefab into your scene. Then drag the "NRVirtualDisplayer" prefab into your scene and modify it according to your needs. For example, add some new buttons or change the style of controller UI to match your application.</font>`

Step by step
~~~~~~~~~~~~


* :raw-html-m2r:`<font color=#636363>Find the "NRVirtualDisplayer" prefab, and drag it to your scene.</font>`

  .. image:: ../../../images/cpc2.png


* :raw-html-m2r:`<font color=#636363>Break this prefab.</font>`

  .. image:: ../../../images/cpc3.png


* :raw-html-m2r:`<font color=#636363>Set the resolution of Game View in Unity Editor to 1080 x 2340.</font>`
 
  .. image:: ../../../images/cpc4.png


* :raw-html-m2r:`<font color=#636363>Double click the "NRVirtualDisplayer" object in Hierarchy to find it in Scene View, then you could design buttons or other UI as you like.</font>`

  .. image:: ../../../images/cpc5.png


* :raw-html-m2r:`<font color=#636363>Make sure three basic NRButton are set. If you don't need them, you could set these objects as false.</font>`

  .. image:: ../../../images/cpc6.png


* :raw-html-m2r:`<font color=#636363>If you want a sample scene, check the  "Input-VirtualController" scene in SDK demos.</font>`


How to Debug a custom virtual controller in Unity Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* :raw-html-m2r:`<font color=#636363>Before clicking play in Unity Editor, the game view resolusion should be set to normal, like 1920*1080. Then click Play, find the "NRInput" object in the scene and enable the "EmulateVirtualDisplayInEditor" checkbox.</font>`

  .. image:: ../../../images/cpc7.png


* :raw-html-m2r:`<font color=#636363>When the checkbox is ticked, a virtual controller will be shown at the right-bottom corner in the Game View. You can interact with this virtual controller with your mouse.</font>`

  .. image:: ../../../images/cpc8.png


* :raw-html-m2r:`<font color=#636363>Be aware that once the emulator is enabled, all the controller button events will be fired from the virtual controller on the game screen. The original way(mouse button emulate controller button) would not work.</font>`

* :raw-html-m2r:`<font color=#636363>If you want to use the original way to emulate controller buttons, you should uncheck the "EmulateVirtualDisplayInEditor" checkbox.</font>`

Not enough?
^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>We offer a demo in NRSDK called Input-VirtualController for this feature, try it yourself!</font>`

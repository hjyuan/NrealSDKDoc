
NRSDK XRPlugin Introduction
===========================

This document describes how to develop MR applications that run on Nreal MR Glasses with NRSDK Unity XR Platform SDK in Unity, and how to simplify the developing process in Unity with XRSDK. The functions SDK provides include sensor fusion, binocular stereo rendering, correction of optical distortion, etc. It is recommended to set up your project as stated in this document, to meet the minimum technical requirements defined in the NRSDK guidelines. 

Before using this SDK, we recommend that you read the \ `Unity XR documentation <https://docs.unity3d.com/Manual/XR.html>`_\  as you need to be familiar with Unity XR. This SDK provides basic XR features that follow the XR specification established by Unity. If your project aims to use Unity’s API or package, you can add this plugin to your project.

SDK package is released in supported format of the Package Manager, and the folder structure is shown as below after SDK is imported:


.. image:: ../../../images/XR/xr1-1.png


`*Please note this is a preview version.`\


SDK Plugin content
------------------

The primary scripts for NRSDK and editor related scripts are included in `Runtime>Scripts`.

The Android runtime library used in NRSDK is included in `Runtime>Android`.

.. image:: ../../../images/XR/xr1-2.png

Enable NRSDK XRPlugin
======================

To use Unity XR related features，you need to enable XRPlugin in Unity.

Unity 2019.3 and newer versions use a new, unified plug-in framework that enables direct integrations for multiple platforms, including NRSDK. 

To enable NRSDK XRPlugin:

1. In the menu, go to `Window->Package Manager`.

.. image:: ../../../images/XR/xr1-3.png

2. Click `+` and choose `Add package from git URL`, fill in https://github.com/nreal-ai/NRSDK-XR-Plugin.git.

.. image:: ../../../images/XR/xr1-4.png
   
.. image:: ../../../images/XR/xr1-5.png


3. After successfully loaded, go to `Edit>Project Settings>XR Plug-in Management`, click the Android tab, and select `NRSDK` to install the NRSDK XR plugin, which enables the XR support. 

.. image:: ../../../images/XR/xr1-6.png

4. After successfully imported, the NRSDK set up window will pop up, click `Fix` or `Accept All`, XR related features in NRSDK will be enabled automatically. (You can also find the window in `NRSDK>Project Tips`)


.. image:: ../../../images/XR/xr1-7.png

At the same time, `USING_XR_SDK` will also be automatically added to `Scripting Define Symbols` in `Player Setting.`

  
.. image:: ../../../images/XR/xr1-8.png


Disable NRSDK XRPlugin
======================

To disable NRSDK XRPlugin，please follow:

1. In the menu, go to `Window>Package Manager`, choose `NRSDK XR Plugin` and click `Remove` at the bottom.
 
.. image:: ../../../images/XR/xr1-9.png


2. After removing, the NRSDK set up window will pop up, click `Fix` or `Accept All`, XR related features in NRSDK will be disabled automatically. (You can also find the window in `NRSDK>Project Tips`)

.. image:: ../../../images/XR/xr1-10.png

At the same time, `USING_XR_SDK` will also be automatically deleted from `Scripting Define Symbols` in `Player Setting.`

API Interface Function List
===========================

Please find APIs provided by UnityXR plugins at \ `Unity User Manual <https://docs.unity3d.com/Manual/VRReference.html>`_

Currently supported modules in NRSDK XRPlugin:


* 
  `XRDisplaySubsystem <https://docs.unity3d.com/Manual/xrsdk-display.html>`_

* 
  `XRNodeState <https://docs.unity3d.com/ScriptReference/XR.XRNodeState.html>`_

* 
  `XRStats <https://docs.unity3d.com/ScriptReference/XR.XRStats.html>`_

Advanced Features
==================

Single Pass
-----------

SinglePass is based on native \ `Unity Single Pass <https://docs.unity3d.com/Manual/SinglePassStereoRendering.html>`_\  feature. It uses one camera to fulfill stereo rendering, reducing half of Draw Call and Occlusion Culling. Using this feature can increase frame rate dramatically in complex scenes. But SinglePass doesn’t support post processing.

MutiView shown below is the original SinglePass mode:

.. image:: ../../../images/XR/xr1-11.png


Performance optimization Guide
==============================

Parameters Suggestions
-----------------------
* The application frame rate should not be less than 45 frames.

* Avoid more than 100,000 triangular patches being rendered in the camera view.

* Avoid more than 100,000 model vertices being rendered in the camera view.

* Avoid more than 50 common unity lights such as electric light sources.

* Avoid more than 50 ordinary unity particle systems.

* Avoid more than 1 pixel light.

* Avoid more than 1.2 render scale.

* Avoid more than 2 shader passes.

* Watch for greater than 2k by 2k texture resolution after LOD bias.

* Avoid slow physics settings such as Sleep Threshold values of less than 0.005, Default Contact Offset values of less than 0.01, or Solver Iteration Count settings greater than 6.




Best Practices
--------------

* Try to use the more efficient Unity shader

* Use trilinear or anisotropic filtering on textures. See Textures in the Unity Manual for more information.

* Use mesh-based occlusion culling (see Occlusion Culling in the Unity Manual).

* Always use the Forward Rendering path (see Forward Rendering Path Details in the Unity Manual).



* Verify that non-static objects with colliders are not missing rigidbodies in themselves or in the parent chain.

* Avoid inefficient effects such as SSAO, motion blur, global fog, parallax mapping.

* Avoid excessive use of multipass shaders (e.g., legacy specular).

* Avoid large textures or using a lot of prefabs in startup scenes (for bootstrap optimization). When using large textures, compress them when possible.

* Avoid realtime global illumination.

* Disable shadows when approaching the geometry or draw call limits.

* Be cautious using Unity WWW and avoid large file downloads. It may be acceptable for very small files.
    
* For Android apps with voice chat, you should use the Microphone APIs to avoid issues with Parties. For more information about Parties, go to the Parties and Party Chat topic.
    
* Enable SinglePass renderring.
    
* Disable Plane Detection or ImageTracking in Settings if not in use.
